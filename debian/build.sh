#!/bin/bash

export BUILD_PLATFORM=LINUX
source MakeTools/funcs-ext.sh

settarget x86_64

export BUILD_WITH_STACK=OPENIB
export BUILD_TARGET=X86_64
export PRODUCT='DPKG'

PKGVERS=$(dpkg-parsechangelog -ldebian/changelog | awk '/^Version:/ {print $2}')
export RELEASE_TAG=${PKGVERS%-*}
export TL_DIR=$PWD
# for HSM the kernel rev is not important.  We simply use the kernel rev
# of the running kernel.  While BUILD_TARGET_OS_VERSION is needed by Makerules
# it will have no impact on what is actually built for HSM
export BUILD_TARGET_OS_VERSION=4.x
setver $BUILD_TARGET_OS_VENDOR $BUILD_TARGET_OS_VERSION
export BUILD_TARGET_OS_ID=4.x-x86_64

cd Esm && ./rpm_runmake

# Check the results of the build for errors and unexpected warnings.
./check_results -r build.res build.err build.warn
